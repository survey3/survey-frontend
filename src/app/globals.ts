
export class GlobalConstants {

    public static VALIDATION_MSGS: object = {
        empty: 'Please enter this field',
        valid: 'Please enter valid field'
    }

    public static VALIDATORS: object = {
        NUMBER: '^[0-9]*$',
        USERNAME: '/^[a-zA-Z0-9]+([a-zA-Z0-9](_|-| )[a-zA-Z0-9])*[a-zA-Z0-9]+$/'
    }
}