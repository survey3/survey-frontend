import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { SharedService } from './services/shared.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'frontend';

  private _success = new Subject<string>();

  message = false;
  staticAlertClosed = false;
  successMessage: string;
  alertType: string = '';

  constructor(
    private _sharedService: SharedService
  ) { }

  ngOnInit() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.successMessage = null);

    this._sharedService.dataSourceForMessage$.subscribe(result => {
      if (result) {
        if (result.status == "warning") {
          this.alertType = 'warning';
        } else if (result.status == "success") {
          this.alertType = 'success';
        } else {
          this.alertType = 'danger';
        }
        this._success.next(result.message);
      }
    });
  }

}
