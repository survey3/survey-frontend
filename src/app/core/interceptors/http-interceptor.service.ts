import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = null;

    token = localStorage.getItem("token");

    if (token === null || token === '') {
      return next.handle(req);
    } else {
      req = req.clone({
        setHeaders: {
          Authorization: token
        }
      });
      return next.handle(req);
    }
  }

}