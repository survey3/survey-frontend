import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './layout/login/login.component';
import { MainComponent } from './layout/main/main.component';
import { AuthGuard } from './core/guards/auth.guard';
import { NavigateGuard } from './core/guards/navigate.guard';
import { SignupComponent } from './layout/signup/signup.component';
import { DashboardComponent } from './layout/dashboard/dashboard.component';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'dashboard', component: DashboardComponent },

  // {
  //   path: '', component: MainComponent,
  //   // canActivate: [NavigateGuard],
  //   children: [
  //     {
  //       path: 'dashboard',
  //       // canActivate: [AuthGuard],
  //       loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
  //     }
  //   ]
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
