import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AccountService } from 'src/app/services/account.service';
import { SharedService } from 'src/app/services/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginSubscription$: Subscription

  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private _accountService: AccountService,
    private _sharedService: SharedService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  profileDetails: object = {}
  onLogin() {
    this.loginSubscription$ = this._accountService.login(this.loginForm.value).subscribe(
      (res) => {
        if (res?.status) {
          localStorage.setItem("token", res.data);
          this._sharedService.sendMessage({ status: "success", message: res.message });
          this.router.navigate(['/dashboard']);
        }
      }, (err) => {
        this._sharedService.sendMessage({ status: "error", message: err['error'].message });
      }
    );
  }



  ngOnDestroy() {
    if (this.loginSubscription$) {
      this.loginSubscription$.unsubscribe();
    }
  }

}
