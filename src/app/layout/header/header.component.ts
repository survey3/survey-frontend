import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AccountService } from 'src/app/services/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  getAccountInfoSubscription$: Subscription;

  constructor(
    private _accountService: AccountService,
    private router: Router,

  ) { }

  profileDetails: object = {};

  ngOnInit(): void {
    this.getAccountInfo();
    this._accountService.accountInfo$.subscribe(
      (res) => {
        this.profileDetails = res;
      }
    );
  }


  getAccountInfo() {
    this.getAccountInfoSubscription$ = this._accountService.getAccountInfo().subscribe(
      (res) => {
        if (res?.status) {
          this._accountService.setAccountInfo(res.data);
        }
      }
    )
  }

  onLogout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }


  ngOnDestroy() {
    if (this.getAccountInfoSubscription$) {
      this.getAccountInfoSubscription$.unsubscribe();
    }
  }

}
