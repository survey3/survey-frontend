import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AccountService } from 'src/app/services/account.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedService } from 'src/app/services/shared.service';
import { GlobalConstants } from 'src/app/globals';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
  signupSubscription$: Subscription

  signupForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private _accountService: AccountService,
    private _sharedService: SharedService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  // genders = ['male', 'female', 'other'];
  genders = [
    { value: 'male', label: 'male' },
    { value: 'female', label: 'female' },
    { value: 'other', label: 'other' },
  ];

  roles = [
    { value: 'respondent', label: 'respondent' },
    { value: 'coordinator', label: 'coordinator' }
  ];

  initForm() {
    this.signupForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      age: ['', [Validators.required, Validators.pattern(GlobalConstants.VALIDATORS['NUMBER'])]],
      gender: ['male', Validators.required],
      role: ['respondent', Validators.required]
    });
  }

  onSignup() {
    this.signupSubscription$ = this._accountService.register(this.signupForm.value).subscribe(
      (res) => {
        if (res?.status) {
          this.signupForm.reset();
          this.router.navigate(['../login']);
          this._sharedService.sendMessage({ status: "success", message: res.message });
        }
      }, (err) => {
        this._sharedService.sendMessage({ status: "error", message: err['error'].message });
      }
    );
  }



  ngOnDestroy() {
    if (this.signupSubscription$) {
      this.signupSubscription$.unsubscribe();
    }
  }

}
