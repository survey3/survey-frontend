import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { AccountService } from 'src/app/services/account.service';
import { SharedService } from 'src/app/services/shared.service';
import { SurveyService } from 'src/app/services/survey.service';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DatePipe } from '@angular/common';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 1500, noPause: true, showIndicators: true } }
  ]
})
export class DashboardComponent implements OnInit, OnDestroy {

  getSurveysByIdSubscription$: Subscription;
  getAllSurveysSubscription$: Subscription;
  addSurveySubscription$: Subscription;

  modalRef: BsModalRef;

  constructor(
    private _accountService: AccountService,
    private _sharedService: SharedService,
    private _surveyService: SurveyService,
    private modalService: BsModalService,
    private datePipe: DatePipe
  ) { }

  profileDetails: object = {};
  ngOnInit(): void {

    // this.getAllSurveys();
    this._accountService.accountInfo$.subscribe(
      (res) => {
        if (res && res.hasOwnProperty('accountId')) {
          this.profileDetails = res;
          const { role, accountId } = res;
          if (role === 'coordinator') {
            this.getAllSurveys();
          }
          if (role === 'respondent') {
            this.getSurveysById(accountId);
          }
        }
      }
    );
  }

  allSurveys = [];
  getAllSurveys() {
    this.getAllSurveysSubscription$ = this._surveyService.getAllSurveys().subscribe(
      (res) => {
        if (res?.status) {
          this.allSurveys = res['data'];
        }
      }
    )
  }
  allSurveysRespondent = [];
  getSurveysById(accountId) {
    const payload = { accountId: accountId };
    this.getSurveysByIdSubscription$ = this._surveyService.getSurveysById(payload).subscribe(
      (res) => {
        if (res?.status) {
          this.allSurveysRespondent = res['data'];
        }
      }
    )
  }

  genders = [
    { value: 'male', label: 'male', checked: true },
    { value: 'female', label: 'female', checked: false },
    { value: 'other', label: 'other', checked: false }
  ];

  roles = [
    { value: 'respondent', label: 'respondent' },
    { value: 'coordinator', label: 'coordinator' }
  ];

  questionTypes = ['text', 'radio', 'checkbox'];
  question: string = '';
  lowRange: any = 20;;
  highRange: any = 30;
  addedQuestions = [];
  selectedQuestionType: string = '';
  option: string = '';
  optionsArr = [];
  expiry: any;

  answer: string = '';


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }


  onAddQuestion() {
    const payload = {
      question: this.question,
      type: this.selectedQuestionType || 'text',
      // options: this.optionsArr.join('|') || null
      options: this.optionsArr.length > 0 ? this.optionsArr : null
    }
    this.addedQuestions.push(payload);
    this.question = '';
    this.selectedQuestionType = '';
    this.optionsArr = [];
    this.option = '';

    console.log(this.addedQuestions);
  }

  onAddOption() {
    this.optionsArr.push(this.option);
    this.option = '';
  }

  onChange(val) {
    this.selectedQuestionType = val;
  }

  getDate(date) {
    if (new Date().getTime() < new Date(date).getTime()) {
      return `Validity : ${this.datePipe.transform(date, 'dd MMM yyyy')}`;
    } else {
      return 'Expired'
    }
  }

  surveyName: string = '';

  onPublish() {
    let forGenders = [];
    this.genders.forEach((ele) => {
      if (ele.checked) {
        forGenders.push(ele.value);
      }
    });

    const payload = {
      surveyName: this.surveyName,
      ageGroup: `${this.lowRange},${this.highRange}`,
      gender: forGenders.join(','),
      expiry: this.expiry,
      questions: this.addedQuestions
    };

    this.addSurveySubscription$ = this._surveyService.addSurvey(payload).subscribe(
      (res) => {
        if (res?.status) {
          this._sharedService.sendMessage({ status: "success", message: res.message });
          this.resetAll();
          this.closeModal();
          this.getAllSurveys();
        } else {
          this._sharedService.sendMessage({ status: "error", message: res.message });
        }
      }
    );
  }

  closeModal() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  resetAll() {
    this.addedQuestions = [];
    this.surveyName = '';
    this.expiry = '';
    this.lowRange = 20;
    this.highRange = 30;
    this.optionsArr = [];
  }


  getIsDisabled() {
    if (this.expiry?.length === 0 || this.addedQuestions.length === 0 || this.surveyName.length === 0) {
      return true;
    }
    return false
  }


  selectedRadio: string = '';
  selectedChecks = [];

  onSubmitSurvey(item) {

    // console.log(this.selectedRadio);
    // console.log(this.selectedChecks);
    console.log(item);

    const payload = [];
    // this._surveyService.sendResponse(payload).subscribe(
    //   (res) => {
    //     if (res?.status) {
    //       this._sharedService.sendMessage({ status: "success", message: res.message });
    //     }
    //   }, (err) => {
    //     this._sharedService.sendMessage({ status: "error", message: err.message });
    //   }
    // )

    // const payload = [
    //   {
    //     surveyId: '',
    //     accountId: '',
    //     questionId: '',
    //     answer: ''
    //   }
    // ]

  }

  getItem(arr) {
    arr.map(ele => ele.answer = '');
    return arr;
  }

  onCheck(ev, item) {
    if (ev) {
      this.selectedChecks.push(item);
    }

  }

  ngOnDestroy() {
    if (this.getSurveysByIdSubscription$) {
      this.getSurveysByIdSubscription$.unsubscribe();
    } if (this.getAllSurveysSubscription$) {
      this.getAllSurveysSubscription$.unsubscribe();
    } if (this.addSurveySubscription$) {
      this.addSurveySubscription$.unsubscribe();
    }
  }
}
