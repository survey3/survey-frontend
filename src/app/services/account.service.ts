import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private accountInfoSource = new BehaviorSubject<any>({});
  accountInfo$ = this.accountInfoSource.asObservable();


  constructor(
    private http: HttpClient
  ) { }

  setAccountInfo(data) {
    this.accountInfoSource.next(data);
  }

  getAccountInfo() {
    return this.http.get<any>(environment.BASE_URL + `account/getAccountInfo`);
  }

  register(payload: object): Observable<any> {
    return this.http.post<any>(environment.BASE_URL + `account/register`, payload);
  }

  login(payload: object): Observable<any> {
    return this.http.post<any>(environment.BASE_URL + `account/login`, payload);
  }



}
