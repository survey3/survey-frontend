import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class SurveyService {

  constructor(
    private http: HttpClient
  ) { }

  getAllSurveys(): Observable<any> {
    return this.http.get<any>(environment.BASE_URL + `survey/getAllSurveys`);
  }

  addSurvey(payload: object): Observable<any> {
    return this.http.post<any>(environment.BASE_URL + `survey/addSurvey`, payload);
  }

  getSurveysById(payload: object): Observable<any> {
    return this.http.post<any>(environment.BASE_URL + `survey/getSurveysById`, payload);
  }

  sendResponse(payload: object): Observable<any> {
    return this.http.put<any>(environment.BASE_URL + `survey/respond`, payload);
  }

}
