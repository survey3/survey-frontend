import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private dataSourceForMessage = new BehaviorSubject<any>(null);
  dataSourceForMessage$ = this.dataSourceForMessage.asObservable();

  constructor() { }

  sendMessage(data: any) {
    this.dataSourceForMessage.next(data);
  }
}
